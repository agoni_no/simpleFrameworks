package com.xunqi.entity.bo;

import lombok.Data;

import java.util.Date;

/**
 * @Description:
 * @Created: with IntelliJ IDEA.
 * @author: 夏沫止水
 * @createTime: 2020-06-07 21:58
 **/

@Data
public class ShopCategory {

    private Long shopCategoryId;
    private String shopCategoryIdName;
    private String shopCategoryDesc;
    private String shopCategoryImg;
    private String lineImg;
    private Integer priority;
    private Integer enableStatus;
    private Date createTime;
    private Date lastEditTime;
    private ShopCategory parent;

}
