package com.xunqi.entity.dto;

import com.xunqi.entity.bo.HeadLine;
import com.xunqi.entity.bo.ShopCategory;
import lombok.Data;

import java.util.List;

/**
 * @Description:
 * @Created: with IntelliJ IDEA.
 * @author: 夏沫止水
 * @createTime: 2020-06-07 22:35
 **/

@Data
public class MainPageInfoDto {

    /**
     * 头条列表
     */
    private List<HeadLine> headLineList;

    /**
     * 分类列表
     */
    private List<ShopCategory> shopCategoryList;

}
