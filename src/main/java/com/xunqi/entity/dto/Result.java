package com.xunqi.entity.dto;

import lombok.Data;

/**
 * @Description:
 * @Created: with IntelliJ IDEA.
 * @author: 夏沫止水
 * @createTime: 2020-06-07 22:26
 **/

@Data
public class Result<T> {

    /**
     * 本次请求结果的状态码.200表示成功
     */
    private Integer code;

    /**
     * 本次请求结果的详情
     */
    private String message;

    /**
     * 本次请求返回的结果集
     */
    private T data;

}
