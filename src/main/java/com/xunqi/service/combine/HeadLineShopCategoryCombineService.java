package com.xunqi.service.combine;

import com.xunqi.entity.dto.MainPageInfoDto;
import com.xunqi.entity.dto.Result;

/**
 * @Description:
 * @Created: with IntelliJ IDEA.
 * @author: 夏沫止水
 * @createTime: 2020-06-07 22:35
 **/
public interface HeadLineShopCategoryCombineService {

    Result<MainPageInfoDto> getMainPageInfo();

}
