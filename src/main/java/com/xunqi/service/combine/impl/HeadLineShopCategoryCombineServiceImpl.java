package com.xunqi.service.combine.impl;

import com.xunqi.entity.bo.HeadLine;
import com.xunqi.entity.bo.ShopCategory;
import com.xunqi.entity.dto.MainPageInfoDto;
import com.xunqi.entity.dto.Result;
import com.xunqi.service.combine.HeadLineShopCategoryCombineService;
import com.xunqi.service.solo.HeadLineService;
import com.xunqi.service.solo.ShopCategoryService;

import java.util.List;

/**
 * @Description:
 * @Created: with IntelliJ IDEA.
 * @author: 夏沫止水
 * @createTime: 2020-06-07 22:36
 **/

public class HeadLineShopCategoryCombineServiceImpl implements HeadLineShopCategoryCombineService {

    private HeadLineService headLineService;

    private ShopCategoryService shopCategoryService;

    @Override
    public Result<MainPageInfoDto> getMainPageInfo() {

        //1、获取头条列表
        HeadLine headLine = new HeadLine();
        headLine.setEnableStatus(1);
        Result<List<HeadLine>> headLineList = headLineService.getHeadLineList(headLine, 1, 4);

        //2、获取店铺类别列表
        ShopCategory shopCategory = new ShopCategory();
        Result<List<ShopCategory>> shopCategoryList = shopCategoryService.getShopCategoryList(shopCategory, 1, 100);

        //3、合并两者并返回
        Result<MainPageInfoDto> result = merageMainPageInfoResult(headLineList,shopCategoryList);

        return result;
    }

    private Result<MainPageInfoDto> merageMainPageInfoResult(Result<List<HeadLine>> headLineList, Result<List<ShopCategory>> shopCategoryList) {



        return null;
    }
}
