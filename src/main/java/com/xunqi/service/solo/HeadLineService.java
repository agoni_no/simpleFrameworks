package com.xunqi.service.solo;

import com.xunqi.entity.bo.HeadLine;
import com.xunqi.entity.dto.Result;

import java.util.List;

/**
 * @Description:
 * @Created: with IntelliJ IDEA.
 * @author: 夏沫止水
 * @createTime: 2020-06-07 22:31
 **/
public interface HeadLineService {

    Result<Boolean> add(HeadLine headLine);

    Result<Boolean> removeHeadLine(HeadLine headLine);

    Result<Boolean> modifyHeadLine(HeadLine headLine);

    Result<HeadLine> getHeadLineById(Long headLineId);

    Result<List<HeadLine>> getHeadLineList(HeadLine headLineCondition, int pageIndex, int pageSize);
}
