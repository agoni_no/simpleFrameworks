package com.xunqi.service.solo;

import com.xunqi.entity.bo.ShopCategory;
import com.xunqi.entity.dto.Result;

import java.util.List;

/**
 * @Description:
 * @Created: with IntelliJ IDEA.
 * @author: 夏沫止水
 * @createTime: 2020-06-07 22:31
 **/
public interface ShopCategoryService {

    Result<Boolean> addShopCategory(ShopCategory shopCategory);

    Result<Boolean> removeShopCategory(ShopCategory shopCategory);

    Result<Boolean> modifyShopCategory(ShopCategory shopCategory);

    Result<ShopCategory> getShopCategoryById(int shopCategoryId);

    Result<List<ShopCategory>> getShopCategoryList(ShopCategory shopCategoryCondition, int pageIndex, int pageSize);

}
