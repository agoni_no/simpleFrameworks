package com.xunqi.service.solo.impl;

import com.xunqi.entity.bo.HeadLine;
import com.xunqi.entity.dto.Result;
import com.xunqi.service.solo.HeadLineService;

import java.util.List;

/**
 * @Description:
 * @Created: with IntelliJ IDEA.
 * @author: 夏沫止水
 * @createTime: 2020-06-07 22:34
 **/
public class HeadLineServiceImpl implements HeadLineService {
    @Override
    public Result<Boolean> add(HeadLine headLine) {
        return null;
    }

    @Override
    public Result<Boolean> removeHeadLine(HeadLine headLine) {
        return null;
    }

    @Override
    public Result<Boolean> modifyHeadLine(HeadLine headLine) {
        return null;
    }

    @Override
    public Result<HeadLine> getHeadLineById(Long headLineId) {
        return null;
    }

    @Override
    public Result<List<HeadLine>> getHeadLineList(HeadLine headLineCondition, int pageIndex, int pageSize) {
        return null;
    }
}
