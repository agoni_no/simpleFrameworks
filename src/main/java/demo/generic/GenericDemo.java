package demo.generic;

import java.util.LinkedList;
import java.util.List;

/**
 * @Description:
 * @Created: with IntelliJ IDEA.
 * @author: 夏沫止水
 * @createTime: 2020-06-07 22:06
 **/
public class GenericDemo {

    public static void main(String[] args) {

        List linkedList = new LinkedList();
        linkedList.add("words");
        linkedList.add(1);
        linkedList.forEach(System.out::println);

    }

}
