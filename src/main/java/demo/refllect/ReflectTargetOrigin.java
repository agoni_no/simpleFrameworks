package demo.refllect;

/**
 * @Description:
 * @Created: with IntelliJ IDEA.
 * @author: 夏沫止水
 * @createTime: 2020-06-12 21:32
 **/
public class ReflectTargetOrigin {

    String defaultMember = "default";

    public String publicMember = "public";

    protected String protectedMember = "protected";

    private String privateMember = "private";

}
